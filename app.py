#!/usr/bin/env python

from functools import wraps
from flask import request, Response, Flask
import string, random

app = Flask(__name__)

def gen_pwd(int_maxlength, bool_IncludeAlphaChars):
    password = []
    for i in range(random.randrange(1, int_maxlength+1)):
        if bool_IncludeAlphaChars:
            if random.randrange(10) % 2 == 0:
                password.append(random.choice(string.ascii_letters))
            else:
                password.append(str(random.randrange(10)))
        else:
            password.append(str(random.randrange(10)))
    return ''.join(password)

_password = gen_pwd(1, True)

def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    return username == 'admin' and _password == password

def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
    'Could not verify your access level for that URL.\n'
    'You have to login with proper credentials', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated

@app.route('/secret-page')
@requires_auth
def secret_page():
    return '<html><body>So what was the correct password again?</body></html>'


if __name__ == '__main__':
  app.run(debug=True)
